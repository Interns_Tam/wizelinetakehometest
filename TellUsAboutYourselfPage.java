import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;

public class TellUsAboutYourselfPage {
	WebDriver driver;
    By firstName = By.id("firstName");
    By lastName = By.id("lastName");
    By emailAddress = By.id("email");
    By monthOfBirth = By.id("birthMonth");
    By dateOfBirth = By.id("birthDay");
    By yearOfBirth = By.id("birthYear");  
    By btnNext = By.xpath("//*[@id=\"regs_container\"]/div/div[2]/div/div[2]/div/form/div[2]/a");
    
    
    public TellUsAboutYourselfPage(WebDriver driver){
        this.driver = driver;
    }

    //Set first name in textbox
    public void setFirstName(String strFirstName){
        driver.findElement(firstName).sendKeys(strFirstName);
    }
    
    public void setLastName(String strLastName){
        driver.findElement(lastName).sendKeys(strLastName);
    }

    //Set password in password textbox
    public void setEmailAddres(String strEmail){
         driver.findElement(emailAddress).sendKeys(strEmail);
    }
    
    public void selectMonthOfBirth(String strMonth){
        Select month = new Select(driver.findElement(monthOfBirth));
        month.selectByVisibleText(strMonth);
   }
    
    public void selectDateOfBirth(String strDate){
        Select date = new Select(driver.findElement(dateOfBirth));
        date.selectByVisibleText(strDate);
   }

    public void selectYearOfBirth(String strYear){
        Select year = new Select(driver.findElement(yearOfBirth));
        year.selectByVisibleText(strYear);
   }
    
    //Click on login button

    public void submitBasicInfo(){
            driver.findElement(btnNext).click();
    }
}


